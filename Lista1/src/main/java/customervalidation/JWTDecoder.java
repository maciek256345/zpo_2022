package customervalidation;

import java.util.Base64;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JWTDecoder {
    private static final String regex = "[A-Z]{2}[0-9]+ex_\\s[\\w]{8}-[\\w]{4}-[\\w]{4}-[\\w]{4}-[\\w]{12}[0-9.]*";

    public String decodeJWT(String token) throws Exception {

        String[] parts = token.split("\\.");
        Base64.Decoder decoder = Base64.getUrlDecoder();
        String payload = new String(decoder.decode(parts[1]));
        Pattern pattern = Pattern.compile("customerToken");
        Matcher matcher = pattern.matcher(payload);
        if (!matcher.find()) {
            throw new Exception("JWT don't contain customerToken !");
        }
        Pattern customerTokenPattern = Pattern.compile(regex);
        Matcher customerTokenMatcher = customerTokenPattern.matcher(payload);
        if (!customerTokenMatcher.find()) {
            throw new Exception("customerToken don't have correct format !");
        }

        // return a customerToken
        return customerTokenMatcher.group(0);
    }

}
