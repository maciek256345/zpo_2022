package customervalidation;

import java.util.ArrayList;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CustomerTokenDecoder {
    private static final String regex = "([A-Z]{2})([0-9]+)ex_\\s([\\w]{8}-[\\w]{4}-[\\w]{4}-[\\w]{4}-[\\w]{12})([0-9.]*)";

    public Customer decode(String customerToken) throws Exception {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(customerToken);
        matcher.find();

        CountryCode countryCode = new CountryCode(matcher.group(1));
        Integer id = Integer.parseInt(matcher.group(2));
        UUID externalId = UUID.fromString((matcher.group(3)));
        String enabledServicesString = matcher.group(4);

        String[] services = enabledServicesString.split("\\.");
        ArrayList<Integer> enabledServices = new ArrayList<>();

        for (String s : services) {
            enabledServices.add(Integer.parseInt(s));
        }

        // return a new RegisteredCustomer object
        return new Customer(countryCode, id, externalId, enabledServices);

    }


}
