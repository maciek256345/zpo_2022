package customervalidation;

public class CountryCode {
    private String code;

    public CountryCode(String code) throws Exception {
        setCode(code);
    }

    public void setCode(String code) throws Exception {
        if (code.length() != 2) {
            throw new Exception("Uncorrect country code !");
        }
        this.code = code;
    }

    public String getCode() {
        return code;
    }

}
