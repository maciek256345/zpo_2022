package customervalidation;

import java.util.List;

public class CustomerTokenValidator {

    public boolean validation(String customerToken, int id) throws Exception {
        CustomerTokenDecoder customerTokenDecoder = new CustomerTokenDecoder();
        Customer customer = customerTokenDecoder.decode(customerToken);
        List<Integer> enabledServices = customer.getEnabledServices();

        // return boolean value if access is granted
        return enabledServices.contains(id);
    }

}
