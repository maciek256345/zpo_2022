package customervalidation;

import javax.crypto.spec.IvParameterSpec;
import java.util.ArrayList;
import java.util.UUID;

public class Customer {
    private CountryCode countryCode;
    private int id;
    private UUID externalId;
    private ArrayList<Integer> enabledServices;
    private String salt;
    private String mainKey;
    private IvParameterSpec iv;


    public Customer(int id, String salt, String mainKey, IvParameterSpec iv) {
        this.id = id;
        this.salt = salt;
        this.mainKey = mainKey;
        this.iv = iv;
    }


    public Customer(CountryCode countryCode, Integer id, UUID externalId, ArrayList<Integer> enabledServices) {
        this.countryCode = countryCode;
        this.id = id;
        this.externalId = externalId;
        this.enabledServices = enabledServices;
    }

    public Customer(int id) {
        this.id = id;
    }

    public CountryCode getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(CountryCode countryCode) {
        this.countryCode = countryCode;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public UUID getExternalId() {
        return externalId;
    }

    public void setExternalId(UUID externalId) {
        this.externalId = externalId;
    }

    public ArrayList<Integer> getEnabledServices() {
        return enabledServices;
    }

    public void setEnabledServices(ArrayList<Integer> enabledServices) {
        this.enabledServices = enabledServices;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getMainKey() {
        return mainKey;
    }

    public void setMainKey(String mainKey) {
        this.mainKey = mainKey;
    }

    public IvParameterSpec getIv() {
        return iv;
    }

    public void setIv(IvParameterSpec iv) {
        this.iv = iv;
    }
}
