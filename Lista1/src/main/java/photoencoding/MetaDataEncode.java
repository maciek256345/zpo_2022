package photoencoding;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.time.LocalDate;

public class MetaDataEncode {


    public static String jsonToXml(String filePath, String outputFile) throws FileNotFoundException {

        JSONParser parser = new JSONParser();
        String xml = "";
        try {
            Object obj = parser.parse(new FileReader(filePath));
            JSONObject json = new JSONObject(obj.toString());

            String author = (String) json.get("author");
            String title = (String) json.get("title");
            String location = (String) json.get("location");
            String date = (String) json.get("date");
            String description = (String) json.get("description");
            String format = (String) json.get("format");

            PhotoWithMetaData photoWithMetaData = new PhotoWithMetaData(author, title, location, LocalDate.parse(date), description, format);

            XStream mapping = new XStream(new DomDriver());
            xml = mapping.toXML(photoWithMetaData);
            PrintWriter zapis = new PrintWriter(outputFile);
            zapis.println(xml);
            zapis.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return xml;
    }

}
