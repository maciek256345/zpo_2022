package photoencoding;

import java.time.LocalDate;

public class PhotoWithMetaData {
    private String author;
    private String title;
    private String location;
    private LocalDate date;
    private String description;
    private String format;
    private Photo photo;


    public PhotoWithMetaData(String author, String title, String location, LocalDate date, String description, String format, Photo photo) {
        this.author = author;
        this.title = title;
        this.location = location;
        this.date = date;
        this.description = description;
        this.format = format;
        this.photo = photo;
    }

    public PhotoWithMetaData(String author, String title, String location, LocalDate date, String description, String format) {
        this.author = author;
        this.title = title;
        this.location = location;
        this.date = date;
        this.description = description;
        this.format = format;
    }

    public String getAuthor() {
        return author;
    }


    public String getTitle() {
        return title;
    }


    public String getLocation() {
        return location;
    }


    public LocalDate getDate() {
        return date;
    }


    public String getDescription() {
        return description;
    }


    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }


    @Override
    public String toString() {
        return "PhotoWithMetaData{" +
                "author='" + author + '\'' +
                ", title='" + title + '\'' +
                ", location='" + location + '\'' +
                ", date=" + date +
                ", description='" + description + '\'' +
                ", format='" + format + '\'' +
                ", photo=" + photo +
                '}';
    }
}
