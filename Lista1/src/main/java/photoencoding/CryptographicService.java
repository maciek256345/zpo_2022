package photoencoding;

import customervalidation.Customer;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;

import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import java.io.*;
import java.time.LocalDate;
import java.util.*;

public class CryptographicService {

    public static void encryptPhoto(String photo, String metadata, String password, int customerId, int photoId) throws Exception {
        //zamiana zdjecia na tablice bajtów
        byte[] bytes = Photo.convertToByteArray(photo);
        //enkodowanie bajtów do base64
        String bytesEncoded = Photo.encode(bytes);
        //pobranie registeredCustomer o zadanym id
        Customer rc = getRegisteredCustomer(customerId);
        //pobranie zaszyfrowanego klucza głównego
        String encryptedMainKey = rc.getMainKey();
        //pobranie iv do odszyfrowania klucza głównego
        IvParameterSpec ivToDecryptMainKey = rc.getIv();
        //klucz do odszyfrowania klucza głównego na podstawie hasła podanego przez customera i salt
        SecretKey secretKeyToDecryptMainKey = AlgorithmAES.getKeyFromPassword(password, rc.getSalt());
        //odszyfrowany klucz główny
        String decryptedMainKey = AlgorithmAES.decrypt("AES/CBC/PKCS5Padding", encryptedMainKey, secretKeyToDecryptMainKey, ivToDecryptMainKey);
        SecretKey secretKeyToEncrypt = AlgorithmAES.convertStringToSecretKeyto(decryptedMainKey);
        IvParameterSpec ivToEncrypt = AlgorithmAES.generateIv();
        String ivToEncryptString = Base64.getMimeEncoder().encodeToString(ivToEncrypt.getIV());
        MetaDataEncode.jsonToXml(metadata, "metadata" + photoId + ".xml");
        //szyfrowanie bajtów zdjęcia
        String encryptedPhoto = AlgorithmAES.encrypt("AES/CBC/PKCS5Padding", bytesEncoded, secretKeyToEncrypt, ivToEncrypt);
        String xmlString = readFile("metadata" + photoId + ".xml", true);
        //szyfrowanie metadanych
        String encryptedMetadata = AlgorithmAES.encrypt("AES/CBC/PKCS5Padding", xmlString, secretKeyToEncrypt, ivToEncrypt);
        String encryptedMetadataWithIV = encryptedMetadata + "\n" + ivToEncryptString;

        save("customer" + customerId + "/metadata" + photoId + ".txt", encryptedMetadataWithIV);
        save("customer" + customerId + "/photo" + photoId + ".txt", encryptedPhoto);

    }

    public static PhotoWithMetaData decryptPhoto(int customerId, String password, String metadata, String photo) throws Exception {

        Customer rc = getRegisteredCustomer(customerId);
        String encryptedMainKey = rc.getMainKey();
        IvParameterSpec ivToDecryptMainKey = rc.getIv();
        SecretKey secretKeyToDecryptMainKey = AlgorithmAES.getKeyFromPassword(password, rc.getSalt());
        String decryptedMainKey = AlgorithmAES.decrypt("AES/CBC/PKCS5Padding", encryptedMainKey, secretKeyToDecryptMainKey, ivToDecryptMainKey);
        SecretKey secretKeyToDecrypt = AlgorithmAES.convertStringToSecretKeyto(decryptedMainKey);

        String encryptedPhoto = readFile(photo, false);

        String IvString = "";
        String encryptedMetadata = "";
        File file = new File(metadata);
        Scanner in = new Scanner(file);

        int k = 0;
        int m = 0;
        while (in.hasNext()) {
            m += 1;
            k += 1;
            if (m == 2) {
                IvString = in.nextLine();
            }
            if (k == 1) {
                encryptedMetadata = in.nextLine();
            }
        }

        IvParameterSpec ivToDecrypt = new IvParameterSpec(Base64.getMimeDecoder().decode(IvString));
        //odszyfrowanie zdjęcia
        String photoDecrypted = AlgorithmAES.decrypt("AES/CBC/PKCS5Padding", encryptedPhoto, secretKeyToDecrypt, ivToDecrypt);
        //odkodowanie z base64
        byte[] imgBytes = Base64.getUrlDecoder().decode(photoDecrypted);
        //odszyfrowanie metadanych
        String metadataDecrypted = AlgorithmAES.decrypt("AES/CBC/PKCS5Padding", encryptedMetadata, secretKeyToDecrypt, ivToDecrypt);
        //przepisanie XML na JSON
        JSONObject obj = XML.toJSONObject(metadataDecrypted);

        JSONObject obj1 = XML.toJSONObject(metadataDecrypted);
        JSONObject object = obj1.getJSONObject("photoencoding.PhotoWithMetaData");

        String author = String.valueOf(object.getString("author"));
        String title = String.valueOf(object.getString("title"));
        String location = String.valueOf(object.getString("location"));
        String date = String.valueOf(object.getString("date"));
        String description = String.valueOf(object.getString("description"));
        String format = String.valueOf(object.getString("format"));

        PhotoWithMetaData photoWithMetaData = new PhotoWithMetaData(author, title, location, LocalDate.parse(date), description, format, new Photo(imgBytes));
        return photoWithMetaData;

    }

    public static void save(String path, String content) throws FileNotFoundException {
        PrintWriter save = new PrintWriter(path);
        save.println(content);
        save.close();
    }


    public static Customer getRegisteredCustomer(int cutomerId) throws IOException {
        String xmlString = readFile("customer.xml", true);

        JSONObject obj = XML.toJSONObject(xmlString);
        JSONObject obj2 = obj.getJSONObject("collection");
        JSONArray array = obj2.getJSONArray("photoencoding.RegisteredCustomer");

        ArrayList<Customer> registeredCustomers = new ArrayList<>();

        for (int n = 0; n < array.length(); n++) {
            JSONObject object = array.getJSONObject(n);
            int customerId = object.getInt(("customerId"));
            String salt = String.valueOf(object.getInt("salt"));
            String mainKey = object.getString("mainKey");
            String iv = object.getString("iv");
            byte[] b = Base64.getUrlDecoder().decode(iv.getBytes());
            IvParameterSpec iv2 = new IvParameterSpec(b);
            Customer r = new Customer(customerId, salt, mainKey, iv2);
            registeredCustomers.add(r);
        }
        int index = 0;
        for (int i = 0; i < registeredCustomers.size(); i++) {
            if (registeredCustomers.get(i).getId() == cutomerId) {
                index = i;
            }
        }
        return registeredCustomers.get(index);
    }

    public static String readFile(String path, boolean newLine) throws FileNotFoundException {
        File file = new File(path);
        Scanner in = new Scanner(file);
        StringBuilder fileString = new StringBuilder();
        while (in.hasNext()) {
            String x = in.nextLine();
            fileString.append(x);
            if (newLine)
                fileString.append("\n");
        }
        return String.valueOf(fileString);
    }

}
