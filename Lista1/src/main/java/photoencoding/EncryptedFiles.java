package photoencoding;

import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;

public class EncryptedFiles {
    private SecretKey secretKey;
    private IvParameterSpec ivp;
    private String encryptedValue1;
    private String encryptedValue2;

    public EncryptedFiles(SecretKey secretKey, IvParameterSpec ivp, String encryptedValue1, String encryptedValue2) {
        this.secretKey = secretKey;
        this.ivp = ivp;
        this.encryptedValue1 = encryptedValue1;
        this.encryptedValue2 = encryptedValue2;
    }

    public EncryptedFiles(SecretKey secretKey, IvParameterSpec ivp, String encryptedValue1) {
        this.secretKey = secretKey;
        this.ivp = ivp;
        this.encryptedValue1 = encryptedValue1;
    }

    public SecretKey getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(SecretKey secretKey) {
        this.secretKey = secretKey;
    }

    public IvParameterSpec getIvp() {
        return ivp;
    }

    public void setIvp(IvParameterSpec ivp) {
        this.ivp = ivp;
    }

    public String getEncryptedValue1() {
        return encryptedValue1;
    }

    public void setEncryptedValue1(String encryptedValue1) {
        this.encryptedValue1 = encryptedValue1;
    }

    public String getEncryptedValue2() {
        return encryptedValue2;
    }

    public void setEncryptedValue2(String encryptedValue2) {
        this.encryptedValue2 = encryptedValue2;
    }
}
