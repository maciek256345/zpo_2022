package photoencoding;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Base64;

public class Photo {
    private byte[] byteArray;


    public Photo(byte[] byteArray) {
        this.byteArray = byteArray;
    }

    public static byte[] convertToByteArray(String path) throws IOException {
        BufferedImage bImage = ImageIO.read(new File(path));
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ImageIO.write(bImage, "jpg", bos);
        bos.toByteArray();
        return bos.toByteArray();
    }

    public static String encode(byte[] bytes) {
        Base64.Encoder encoder = Base64.getUrlEncoder();
        return encoder.encodeToString(bytes);
    }


}


