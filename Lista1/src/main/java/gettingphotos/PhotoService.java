package gettingphotos;

import photoencoding.PhotoWithMetaData;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class PhotoService {

    public static List<PhotoWithMetaData> photoFilter(int customerId, String password,
                                                      String author, String location, String title, String description,
                                                      LocalDate startDate, LocalDate endDate) throws Exception {

        List<String> metadata = new ArrayList<>();
        List<String> photos = new ArrayList<>();
        FileService.getAllPhotos(customerId, metadata, photos);

        List<PhotoWithMetaData> photoList = FileService.decryptAllPhotos(metadata, photos, customerId, password);
        List<PhotoWithMetaData> photoListFiltered = new ArrayList<>();

        photoListFiltered = photoList.stream().filter(
                x -> {
                    boolean isAuthorAndLocation = x.getAuthor().equalsIgnoreCase(author) &&
                            x.getLocation().equalsIgnoreCase(location);
                    if (!isAuthorAndLocation) {
                        return false;
                    }
                    boolean isTitleAndDescription = x.getTitle().toLowerCase().contains(title.toLowerCase()) &&
                            x.getDescription().toLowerCase().contains(description.toLowerCase());
                    if (!isTitleAndDescription) {
                        return false;
                    }
                    boolean correctDate = startDate == null && endDate == null;

                    if (startDate != null && endDate != null) {
                        correctDate = x.getDate().isBefore(endDate) && x.getDate().isAfter(startDate);

                    }
                    if (startDate == null && endDate != null) {
                        correctDate = x.getDate().isBefore(endDate);
                    }

                    if (endDate == null && startDate != null) {
                        correctDate = x.getDate().isAfter(startDate);
                    }
                    return correctDate;
                }).collect(Collectors.toList());

        return photoListFiltered;

    }

}
