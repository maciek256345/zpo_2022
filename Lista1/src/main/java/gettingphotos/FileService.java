package gettingphotos;

import photoencoding.CryptographicService;
import photoencoding.PhotoWithMetaData;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FileService {

    public static void getAllPhotos(int customerId, List<String> metadataList, List<String> photosList) {

        File folder = new File("customer" + customerId);
        File[] listOfFiles = folder.listFiles();

        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                if (listOfFiles[i].getName().contains("meta")) {
                    metadataList.add(listOfFiles[i].getName());

                } else {
                    photosList.add(listOfFiles[i].getName());
                }
            }
        }

    }

    public static ArrayList<PhotoWithMetaData> decryptAllPhotos(List<String> metadata, List<String> photo, int customerId, String password) throws Exception {
        ArrayList<PhotoWithMetaData> photosList = new ArrayList<>();
        for (int i = 0; i < metadata.size(); i++) {
            String meta = "customer" + customerId + "/" + metadata.get(i);
            String pho = "customer" + customerId + "/" + photo.get(i);


            photosList.add(CryptographicService.decryptPhoto(customerId, password, meta, pho));

        }

        return photosList;
    }


}
