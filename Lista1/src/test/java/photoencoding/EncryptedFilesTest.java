package photoencoding;

import customervalidation.Customer;
import org.json.simple.parser.ParseException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;

import static org.junit.Assert.assertEquals;

public class EncryptedFilesTest {

    @Before
    public void setUp() throws Exception {
        System.out.println("New test is about to begin ...");
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("Test ended !");
    }

    Customer rc = CryptographicService.getRegisteredCustomer(11);
    //odszyforwanie klucza głównego
    String encryptedMainKey = rc.getMainKey();
    IvParameterSpec ivToDecryptMainKey = rc.getIv();
    SecretKey secretKeyToDecryptMainKey = AlgorithmAES.getKeyFromPassword("haslo", rc.getSalt());
    String decryptedMainKey = AlgorithmAES.decrypt("AES/CBC/PKCS5Padding", encryptedMainKey, secretKeyToDecryptMainKey, ivToDecryptMainKey);
    SecretKey secretKey = AlgorithmAES.convertStringToSecretKeyto(decryptedMainKey);
    String IVString = "O0_haI64UkTcc_IgrbXrvg==";
    Base64.Decoder decoder = Base64.getUrlDecoder();
    byte[] backToBytes = decoder.decode(IVString);
    IvParameterSpec ivp = new IvParameterSpec(backToBytes);

    EncryptedFiles ef = new EncryptedFiles(secretKey, ivp, "Encrypted Photo.txt", "Encrypted Metadata.txt");


    public EncryptedFilesTest() throws IOException, ParseException, NoSuchAlgorithmException, InvalidKeySpecException, InvalidAlgorithmParameterException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException {
    }


    @Test
    public void getSecretKey() throws NoSuchAlgorithmException {
        SecretKey key = ef.getSecretKey();
        String keyString = AlgorithmAES.convertSecretKeyToString(key);
        assertEquals("fN6AQIEsbxceKCGz1r1umYQAoM5LUBcvyD3WUfIwwI8=", keyString);
    }

    @Test
    public void getIvp() {
        IvParameterSpec ivp = ef.getIvp();
        String ivpString = Base64.getUrlEncoder().encodeToString(ivp.getIV());
        assertEquals("O0_haI64UkTcc_IgrbXrvg==", ivpString);
    }

    @Test
    public void getEncryptedPhoto() {
        assertEquals("Encrypted Photo.txt", ef.getEncryptedValue1());
    }

    @Test
    public void getEncryptedMetadata() {
        assertEquals("Encrypted Metadata.txt", ef.getEncryptedValue2());
    }
}