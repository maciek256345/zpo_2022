package photoencoding;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PhotoTest {

    @Before
    public void setUp() throws Exception {
        System.out.println("New test is about to begin ...");
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("Test ended !");
    }

    @Test
    public void encode() {
        byte[] bytes = new byte[]{1, 2, 3, 4, 5};
        String actual = Photo.encode(bytes);
        assertEquals("AQIDBAU=", actual);
    }
}