package photoencoding;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import static org.junit.Assert.assertEquals;

public class AlgorithmAESTest {

    @Before
    public void setUp() throws Exception {
        System.out.println("New test is about to begin ...");
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("Test ended !");
    }

    @Test
    public void encryption_decryption()
            throws NoSuchAlgorithmException, IllegalBlockSizeException, InvalidKeyException,
            BadPaddingException, InvalidAlgorithmParameterException, NoSuchPaddingException, InvalidKeySpecException {

        String input = "baeldung";
        SecretKey key = AlgorithmAES.getKeyFromPassword("haslo123", "1235");
        IvParameterSpec ivParameterSpec = AlgorithmAES.generateIv();
        String algorithm = "AES/CBC/PKCS5Padding";
        String cipherText = AlgorithmAES.encrypt(algorithm, input, key, ivParameterSpec);
        String plainText = AlgorithmAES.decrypt(algorithm, cipherText, key, ivParameterSpec);
        assertEquals(input, plainText);
    }
}