package photoencoding;

import customervalidation.Customer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.crypto.spec.IvParameterSpec;
import java.io.IOException;
import java.util.Base64;

import static org.junit.Assert.assertEquals;

public class RegisteredCustomerTest {

    Customer c = CryptographicService.getRegisteredCustomer(11);

    public RegisteredCustomerTest() throws IOException {
    }

    @Before
    public void setUp() throws Exception {
        System.out.println("New test is about to begin ...");
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("Test ended !");
    }

    @Test
    public void getMainKey() {
        assertEquals("On1fjyxOFdR49+otD0jk/eS5OkZ+DyfPsveEIfBd4ZAzXe+jMAWx4Uj4GHF7apyf", c.getMainKey());
    }

    @Test
    public void getId() {
        int actual = c.getId();
        assertEquals(11, actual);
    }

    @Test
    public void getSalt() {
        String actual = c.getSalt();
        assertEquals("61493", actual);
    }

    @Test
    public void getIv() {
        IvParameterSpec iv = c.getIv();
        String ivString = Base64.getUrlEncoder().encodeToString(iv.getIV());
        assertEquals("jqO3UYiLzLXEwmE8tJ9ZaQ==", ivString);
    }


}