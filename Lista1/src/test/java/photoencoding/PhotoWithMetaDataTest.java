package photoencoding;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.time.LocalDate;

import static org.junit.Assert.assertEquals;

public class PhotoWithMetaDataTest {
    LocalDate date = LocalDate.of(2022, 2, 23);

    PhotoWithMetaData photoWithMetaData = new PhotoWithMetaData("Maciej", "Photo", "Wroclaw",
            date, "My best photo in Wroclaw", "jpg",
            new Photo(new byte[100]));

    @Before
    public void setUp() throws Exception {
        System.out.println("New test is about to begin ...");
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("Test ended !");
    }

    @Test
    public void getFormat() {
        assertEquals("jpg", photoWithMetaData.getFormat());
    }

    @Test
    public void setFormat() {
        photoWithMetaData.setFormat("png");
        assertEquals("png", photoWithMetaData.getFormat());
    }


    @Test
    public void convertMetadataJsonToXml() throws FileNotFoundException {
        String xmlMetadata = MetaDataEncode.jsonToXml("metadata1.json", "metadata.xml");
        String expected = "<photoencoding.PhotoWithMetaData>\n" +
                "  <author>Maciej</author>\n" +
                "  <title>My dog</title>\n" +
                "  <location>Gdynia</location>\n" +
                "  <date>2021-04-20</date>\n" +
                "  <description>My dog favourite photo</description>\n" +
                "  <format>jpg</format>\n" +
                "</photoencoding.PhotoWithMetaData>";
        assertEquals(expected, xmlMetadata);
    }
}