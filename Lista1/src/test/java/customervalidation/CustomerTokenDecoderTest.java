package customervalidation;

import customervalidation.CountryCode;
import customervalidation.Customer;
import customervalidation.CustomerTokenDecoder;
import customervalidation.JWTDecoder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.UUID;

import static org.junit.Assert.*;

public class CustomerTokenDecoderTest {
    String JWT = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9." +
            "eyJjdXN0b21lclRva2VuIjoiUEw0MzI1ZXhfIGZiYT" +
            "ExMGQwLTQ4NDgtNGRjMC05ZThkLWIxMTQxY2ZkZDY0Z" +
            "TEyLjQzLjU0LjYuMi4xIiwibmFtZSI6IlBXciIsIml" +
            "hdCI6MTUxNjIzOTAyMn0." +
            "ikK6v4yFhXnBf5M9ZYJi1cBVajEnwuu9dgYuWZYYE7g";

    @Before
    public void setUp() throws Exception {
        System.out.println("New test is about to begin ...");
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("Test ended !");
    }

    @Test
    public void decode() throws Exception {
        JWTDecoder jwtDecoder = new JWTDecoder();
        CustomerTokenDecoder customerTokenDecoder = new CustomerTokenDecoder();
        CountryCode countryCode = new CountryCode("PL");
        UUID externalId = UUID.fromString("fba110d0-4848-4dc0-9e8d-b1141cfdd64e");
        ArrayList<Integer> services = new ArrayList<>();
        services.add(12);
        services.add(43);
        services.add(54);
        services.add(6);
        services.add(2);
        services.add(1);

        Customer expectedCustomer = new Customer(countryCode, 4325,
                externalId, services);
        Customer actualCustomer = null;

        try {
            actualCustomer = customerTokenDecoder.decode(jwtDecoder.decodeJWT(JWT));
        } catch (Exception e) {
            e.printStackTrace();
        }

        assertEquals(expectedCustomer.getCountryCode().getCode(), actualCustomer.getCountryCode().getCode());
        assertEquals(expectedCustomer.getId(), actualCustomer.getId());
        assertEquals(expectedCustomer.getExternalId(), actualCustomer.getExternalId());
        assertEquals(expectedCustomer.getEnabledServices(), actualCustomer.getEnabledServices());
    }
}