package customervalidation;

import customervalidation.JWTDecoder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class JWTDecoderTest {
    String JWTWithCustomerToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9." +
            "eyJjdXN0b21lclRva2VuIjoiUEw0MzI1ZXhfIGZiYT" +
            "ExMGQwLTQ4NDgtNGRjMC05ZThkLWIxMTQxY2ZkZDY0Z" +
            "TEyLjQzLjU0LjYuMi4xIiwibmFtZSI6IlBXciIsIml" +
            "hdCI6MTUxNjIzOTAyMn0." +
            "ikK6v4yFhXnBf5M9ZYJi1cBVajEnwuu9dgYuWZYYE7g";

    String JWTWithoutCustomerToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9" +
            ".eyJjdXN0b2lclRva2VuIjoiUEw0MzI1ZXhfIGZiYTExMGQwLTQ4NDgtNGRj" +
            "MC05ZThkLWIxMTQxY2ZkZDY0ZTEyLjQzLjU0LjYuMi4xIiwibmFtZSI6IlBX" +
            "ciIsImlhdCI6MTUxNjIzOTAyMn0.ikK6v4yFhXnBf5M9ZYJi1cBVajEnwuu9" +
            "dgYuWZYYE7g";

    String JWTUncorrectFormat = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9." +
            "eyJjdXN0b21lclRva2VuIjoiUEw0MzI1ZXh4XyBmYmExMTBkMC00ODQ4LT" +
            "RkYzAtOWU4ZC1iMTE0MWNmZGQ2NGUxMi40My41NC42LjIuMSIsIm5hbWUiO" +
            "iJQV3IiLCJpYXQiOjE1MTYyMzkwMjJ9.ikK6v4yFhXnBf5M9ZYJi1cBVajE" +
            "nwuu9dgYuWZYYE7g";

    @Before
    public void setUp() throws Exception {
        System.out.println("New test is about to begin ...");
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("Test ended !");
    }

    @Test
    public void decodeCorrect() {
        JWTDecoder jwtDecoder = new JWTDecoder();
        String customerToken = "";
        try {
            customerToken = jwtDecoder.decodeJWT(JWTWithCustomerToken);
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertEquals("PL4325ex_ fba110d0-4848-4dc0-9e8d-b1141cfdd64e12.43.54.6.2.1", customerToken);
    }

    @Test
    public void decodeNoCustomerToken() {
        JWTDecoder jwtDecoder = new JWTDecoder();
        String actualMessage = "";
        String expectedMessage = "JWT don't contain customerToken !";
        try {
            jwtDecoder.decodeJWT(JWTWithoutCustomerToken);
        } catch (Exception e) {
            actualMessage = e.getMessage();
        }
        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    public void decodeBadCustomerTokenFormat() {
        JWTDecoder jwtDecoder = new JWTDecoder();
        String actualMessage = "";
        String expectedMessage = "customerToken don't have correct format !";
        try {
            jwtDecoder.decodeJWT(JWTUncorrectFormat);
        } catch (Exception e) {
            actualMessage = e.getMessage();
        }
        assertEquals(expectedMessage, actualMessage);
    }
}
