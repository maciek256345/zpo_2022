package customervalidation;

import customervalidation.ServiceConfig;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CryptographicServiceConfigTest {

    @Before
    public void setUp() throws Exception {
        System.out.println("New test is about to begin ...");
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("Test ended !");
    }

    @Test
    public void getId() {
        ServiceConfig serviceConfig = new ServiceConfig();
        int id = serviceConfig.getId();
        assertEquals(12, id);
    }
}