package customervalidation;

import customervalidation.CustomerTokenValidator;
import customervalidation.JWTDecoder;
import customervalidation.ServiceConfig;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CustomerTokenValidatorTest {

    String JWT = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9." +
            "eyJjdXN0b21lclRva2VuIjoiUEw0MzI1ZXhfIGZiYT" +
            "ExMGQwLTQ4NDgtNGRjMC05ZThkLWIxMTQxY2ZkZDY0Z" +
            "TEyLjQzLjU0LjYuMi4xIiwibmFtZSI6IlBXciIsIml" +
            "hdCI6MTUxNjIzOTAyMn0." +
            "ikK6v4yFhXnBf5M9ZYJi1cBVajEnwuu9dgYuWZYYE7g";

    @Before
    public void setUp() throws Exception {
        System.out.println("New test is about to begin ...");
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("Test ended !");
    }


    @Test
    public void validationAcces() throws Exception {
        JWTDecoder jwtDecoder = new JWTDecoder();
        CustomerTokenValidator customerTokenValidator = new CustomerTokenValidator();
        String customerToken = jwtDecoder.decodeJWT(JWT);
        ServiceConfig serviceConfig = new ServiceConfig();
        boolean hasAcces = customerTokenValidator.validation(customerToken, serviceConfig.getId());

        assertTrue(hasAcces);
    }

    @Test
    public void validationNoAcces() throws Exception {
        JWTDecoder jwtDecoder = new JWTDecoder();
        CustomerTokenValidator customerTokenValidator = new CustomerTokenValidator();
        String customerToken = jwtDecoder.decodeJWT(JWT);
        boolean hasAcces = customerTokenValidator.validation(customerToken, 13);

        assertFalse(hasAcces);
    }
}