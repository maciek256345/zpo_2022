package gettingphotos;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import photoencoding.CryptographicService;
import photoencoding.PhotoWithMetaData;

import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.*;

public class PhotoServiceTest {


    @Before
    public void setUp() throws Exception {
        System.out.println("New test is about to begin ...");
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("Test ended !");
    }

    @Test
    public void photoFilter() throws Exception {
        CryptographicService.encryptPhoto("photos/dog.jpg", "metadata1.json", "haslo", 10, 1);
        CryptographicService.encryptPhoto("photos/cat.jpg", "metadata2.json", "haslo", 10, 2);
        CryptographicService.encryptPhoto("photos/elephant.jpg", "metadata3.json", "haslo", 10, 3);
        CryptographicService.encryptPhoto("photos/tiger.jpg", "metadata4.json", "haslo", 10, 4);
        CryptographicService.encryptPhoto("photos/horse.jpg", "metadata5.json", "haslo", 10, 5);

        List<PhotoWithMetaData> list = PhotoService.photoFilter(10, "haslo", "MaCiEJ", "Gdynia", "My", "FAVOURITE", LocalDate.of(2020, 8, 15), null);
        List<PhotoWithMetaData> list2 = PhotoService.photoFilter(10, "haslo", "heleNA", "Wroclaw", "Elephant", "ZOO", null, null);

        assertEquals("My dog favourite photo", list.get(0).getDescription());
        assertEquals(1, list2.size());

    }
}